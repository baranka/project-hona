#!/usr/bin/python
import pygame
import configparser

pygame.init()
#try:
#    sprited=pygame.image.load("img/pildid.png").convert()
#except pygame.error:
#    print("ei leia pildi faili pildid.png")
#    raise SystemExit
#ekraan = pygame.display.set_mode((1280,960))


def get_image(posx, posy, width, height, sprite_sheet):
    image = pygame.Surface([width, height],pygame.SRCALPHA)
    image.blit(sprite_sheet, (0, 0), (posx, posy, width, height))
    return image

def loendur(pilt):
    sprited=pygame.image.load("img\pildid.png")
    x=0
    y=0
    võtmed=[".","c","x","f","e","d","g","h","i","j","k","l","m","o","p","q","r","s","v","w"]
    pildid={}
    for n in range(20):
        pildid[võtmed[n]]=get_image(x,y,32,32,sprited)
        x+=32
    return pildid[pilt]  

def saa_taust(tase):
    parser = configparser.ConfigParser()
    parser.read("level.map")
    taseme_põhi= parser.get("level",tase)
    return taseme_põhi
    

def joonista_taust(juhis):
    asukoht_x=0
    asukoht_y=0
    taust = pygame.Surface([1280,960])
    for x in juhis:
        if x==("\n"):
            continue
        ruut=pygame.transform.scale(loendur(x),(40,40))
        taust.blit(ruut,(asukoht_x,asukoht_y))
        asukoht_x+=40
        if asukoht_x == 1280:
            asukoht_x=0
            asukoht_y+=40
        if asukoht_y == 960:
            break
    return taust




class player(pygame.sprite.DirtySprite):

    animatsioon=0
    y=480
    x=640
    liikumine = [pygame.image.load("img\sina.png"),pygame.image.load("img\sina1.png"),
             pygame.image.load("img\sina2.png"),pygame.image.load("img\sina3.png"),
             pygame.image.load("img\sina4.png"),pygame.image.load("img\sina5.png"),
             pygame.image.load("img\sina6.png"),pygame.image.load("img\sina7.png"),
             pygame.image.load("img\sina8.png"),pygame.image.load("img\sina9.png")]

    
    def __init__(self):
        
        pygame.sprite.DirtySprite.__init__(self)
        self.image=pygame.transform.scale(self.liikumine[0],(64,64))
        self.last_update=pygame.time.get_ticks()
        self.x=640
        self.y=480
        self.rect=self.image.get_rect(center=(self.x,self.y))
        self.rect.move(self.x,self.y)
        self.suund=0
    
        
    def liigu(self,x,y,kokku):
        
        
       
        if kokku==True:
            self.y=self.y-6*y
            self.x=self.x-6*x
        else:
            self.y=self.y+3*y
            self.x=self.x+3*x
        self.rect=self.image.get_rect(topleft=(self.x,self.y))
        #liiguta sprite kasti collisioni detectimiseks
        #self.rect.move(sinna,tänna)
    	
        
 
        
        praegu = pygame.time.get_ticks()
        vasakule=[5,6,7,6,5,8,9,8,5]
        paremale=[0,1,2,1,0,3,4,3,0]
        
        if praegu - self.last_update>30:
            self.last_update=pygame.time.get_ticks()
            if x==1:
                self.image=pygame.transform.scale(self.liikumine[paremale[self.animatsioon]],(64,64))
                self.animatsioon+=1
                self.suund=0
                if self.animatsioon==8:
                    self.animatsioon=0
            elif x==-1:
                self.image=pygame.transform.scale(self.liikumine[vasakule[self.animatsioon]],(64,64))
                self.animatsioon+=1
                self.suund=5
                if self.animatsioon==8:
                    self.animatsioon=0
            elif y==1 and x==0:
                self.image=pygame.transform.scale(self.liikumine[vasakule[self.animatsioon]],(64,64))
                self.animatsioon+=1
                self.suund=5
                if self.animatsioon==8:
                    self.animatsioon=0
            elif y==-1 and x==0:
                self.image=pygame.transform.scale(self.liikumine[paremale[self.animatsioon]],(64,64))
                self.animatsioon+=1
                self.suund=0
                if self.animatsioon==8:
                    self.animatsioon=0
            if x==0 and y==0:
                self.image=pygame.transform.scale(self.liikumine[self.suund],(64,64))

    def joonista(self,x,y,kuhu):
        
        kuhu.blit(self.image,(x,y))
        
        
    def uuenda(self,x,y):
        
        self.liigu(x,y)

class timer():
    
    def __init__(self):

        self.aega_järgi=30
        pygame.time.set_timer(pygame.USEREVENT,1000)
        self.kell=pygame.time.Clock()
        self.font = pygame.font.SysFont('Consolas', 30)
        self.counter, self.text = self.aega_järgi, "10".rjust(3)
    
    def uuenda_kella(self):
        
        self.counter -= 1
        self.text = "Aeg: " + str(self.counter).rjust(3) if self.counter > 0 else "Aeg läbi!"

    def näita(self,asukoht):
        
        asukoht.blit(self.font.render(self.text,True,(0,0,0)),(30,30))
        self.kell.tick(60)
        
    def lisa_aeg(self,arv):
        self.counter=self.counter+arv
        
    def aeg_läbi(self):
        läbi=self.counter
        return läbi


class skoor():
    
    def __init__(self):
        self.skoor=0
        self.tekst="Skoor:" + str(self.skoor)
        self.font = pygame.font.SysFont('Consolas', 30)
    
    def liida(self,summa):
        self.skoor=self.skoor+summa
        self.tekst="Skoor: "+str(self.skoor)
        
    def näita_skoori(self, asukoht):
        
        asukoht.blit(self.font.render(self.tekst, True, (0,0,0)),(1000,30))
        
    def lõpp_tulemus(self):
        return self.skoor


class põõsas(pygame.sprite.Sprite):
    
    def __init__(self,x,y):
        pygame.sprite.Sprite.__init__(self)
        self.image_ruut=loendur("c").convert_alpha()
        self.image=pygame.transform.scale(self.image_ruut,(40,40))
        self.rect=self.image.get_rect(topleft=(x,y))
        
class nuudel(pygame.sprite.Sprite):
    def __init__(self,x,y):
        pygame.sprite.Sprite.__init__(self)
        self.image_ruut=loendur("m").convert_alpha()
        self.image=pygame.transform.scale(self.image_ruut,(40,40))
        self.rect=self.image.get_rect(topleft=(x,y))

class taara(pygame.sprite.Sprite):
    def __init__(self,x,y):
        pygame.sprite.Sprite.__init__(self)
        self.image_ruut=loendur("l").convert_alpha()
        self.image=pygame.transform.scale(self.image_ruut,(40,40))
        self.rect=self.image.get_rect(topleft=(x,y))

def joonista_esemed(teine_kiht,ese,väärtus):
    
    asukoht_x=0
    asukoht_y=0
    eseme_grupp=pygame.sprite.Group()

    for x in teine_kiht:
        if x==väärtus:
            meie_sprite=ese(asukoht_x,asukoht_y)
            meie_sprite.add(eseme_grupp)
        elif x=="\n":
            continue
        else:
            pass

        asukoht_x=asukoht_x+40

        if asukoht_x == 1280:
            asukoht_x=0
            asukoht_y=asukoht_y+40

        if asukoht_y == 960:
            break
    
    return eseme_grupp
        
põõsad=joonista_esemed(saa_taust("kiht1"),põõsas,"c")

pudelid=joonista_esemed(saa_taust("kiht1"),taara,"l")

nuudlid=joonista_esemed(saa_taust("kiht1"),nuudel,"m")




        
        
def skoorid():
    f=open("skoorid.txt","a")
    nimi= input("Sisesta oma nimi: ")
    tekst=f.read()
    tekst=tekst.split(" \n")
    nimekiri={}

    for x in range(len(tekst)):
        if x==1 or x%2==1:
            continue
        
        nimekiri[tekst[x]]=tekst[x+1]
    nimekiri[nimi]=str(skoor.lõpp_tulemus())+"\n"
    uued_skoorid=""
    for x in nimekiri:
        uued_skoorid.append(x)
        uued_skoorid.append(nimekiri[x])
    f.write(uued_skoorid)
    f.close()

    





taust1= pygame.Surface([1280,960])

tegelane=player()

tegelase_group=pygame.sprite.GroupSingle(player())
tegelane.add(tegelase_group)

taust1.blit(joonista_taust(saa_taust("kaart1")),(0,0))
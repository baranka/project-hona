import pygame
import time
from tkinter import *
from tkinter import messagebox
from random import *
pygame.init()

ekraan = pygame.display.set_mode((1280,960))

from pildilaadija import *

FPS=60
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)

font = pygame.font.Font(None, 36)
done = False
display_instructions = True
instruction_page = 1
kell=timer()
logimuusika = pygame.mixer.Sound('data/Su Turno.ogg')

while not done and display_instructions:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.MOUSEBUTTONDOWN:
            instruction_page += 1
            if instruction_page == 2:
                display_instructions = False
    # Set the screen background
    ekraan.fill(BLACK)
 
    if instruction_page == 1:
        logimuusika.play()
        # Draw instructions, page 1
        text = font.render("Teretulemast surelik!", True, RED)
        ekraan.blit(text, [500, 10])
 
        text = font.render("Sul oli eile eksam ja selle tähistamiskes", True, WHITE)
        ekraan.blit(text, [350, 140])
        text = font.render("pidasite kursusega maha kõva peo, ärkad valutava pea", True, WHITE)
        ekraan.blit(text, [350, 170])
        text = font.render("ja tühja kõhuga. Aga nagu üliõpilasele kombeks, oled kogu", True, WHITE)
        ekraan.blit(text, [350, 200])
        text = font.render("raha maha joonud ja nüüd oled teadmatuses mis teha.", True, WHITE)
        ekraan.blit(text, [350, 230])
        text = font.render("Roomad laua alt välja, kes teab kus ja leiad, et kõik", True, WHITE)
        ekraan.blit(text, [350, 260])
        text = font.render("krõpsud on läinud ja kartulisalat samuti. Selle eest oled ", True, WHITE)
        ekraan.blit(text, [350, 290])
        text = font.render("tark ja sul tuleb idee. Terve aed on taarat täis,", True, WHITE)
        ekraan.blit(text, [350, 320])
        text = font.render("mõtled need ära viia, et osta pakk nuudleid ja peavalurohtu.", True, WHITE)
        ekraan.blit(text, [350, 350])
        text = font.render("Kõik alles magavad aga ei tea kauaks, seega sul pole palju aega.", True, WHITE)
        ekraan.blit(text, [350, 380])
        text = font.render("Jookse kiirelt ja korja kokku nii palju pudeleid, kui jõuad.", True, WHITE)
        ekraan.blit(text, [350, 410])
        text = font.render("KUI OLED VALMIS, VAJUTA VASAKUT HIIREKLÕPSU", True, RED)
        ekraan.blit(text, [350, 700])
 
    # Updateb ekraani sellega, mis if sees
    pygame.display.flip()
logimuusika.stop()
pygame.display.set_caption("Project Hona")
muusika = pygame.mixer.Sound('data/pane_valmis.ogg')
muusika.play()
mäng_läbi = False

joonista_taust(saa_taust("kaart1"))

kell=timer()
highskoor=skoor()
kokkupõrge=False        

#põhi loop

while mäng_läbi != True:

    ekraan.blit(taust1,(0,0))

    põõsad.draw(ekraan)
    põõsad.update()
    nuudlid.draw(ekraan)
    pudelid.draw(ekraan)
    nuudlid.update()
    pudelid.update()

    if pygame.sprite.spritecollide(tegelane, põõsad,False):
        kokkupõrge=True
    else:
        kokkupõrge=False

    if pygame.sprite.spritecollide(tegelane, pudelid,True):
        suvaline_arv=randint(0,4)
        highskoor.liida(50)
        if suvaline_arv!=3:
            uus_pudel=taara(randint(40,1200),randint(40,880))
            uus_pudel.add(pudelid)
    
    if pygame.sprite.spritecollide(tegelane, nuudlid,True):
        kell.lisa_aeg(5)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            mäng_läbi = True
        if event.type == pygame.USEREVENT:
            kell.uuenda_kella()

    print(kell.aeg_läbi())
    if kell.aeg_läbi()==0:
        ekraan.fill(BLACK)
        mäng_läbi=True
        pass
    
    if mäng_läbi == True:
        # If game over is true, draw game over
        text = font.render("Aeg otsas", True, WHITE)
        ekraan.blit(text, [490, 400])
        text = font.render("Sinu skoor:" + str(pudelid), True, WHITE)
        ekraan.blit(text, [490, 450])
        
    klahv = pygame.key.get_pressed()
    
    if pygame.key.get_focused()==True:
        if klahv[pygame.K_w]:
            tegelane.liigu(x=0,y=-1,kokku=kokkupõrge)
        if klahv[pygame.K_s]:
            tegelane.liigu(x=0,y=1,kokku=kokkupõrge)
        if klahv[pygame.K_a]:
            tegelane.liigu(x=-1,y=0,kokku=kokkupõrge)
        if klahv[pygame.K_d]:
            tegelane.liigu(x=1,y=0,kokku=kokkupõrge)
    else:
        tegelane.liigu(x=0,y=0,kokku=kokkupõrge)
    kokkupõrge=False
    
    tegelase_group.draw(ekraan)
    
    kell.näita(ekraan)
    highskoor.näita_skoori(ekraan)
    pygame.display.update()

skoorid()
    
pygame.quit()